(function ($, Drupal) {
  Drupal.behaviors.initJsGrid = {
    attach: function (context, settings) {
      $('.data-table__content', context).once('table-initialised').each(function(index, item){
        $container = $(item);

        const data = $container.data('rows');
        const fields = $container.data('fields');

        $container.jsGrid({
          width: "100%",

          inserting: false,
          editing: true,
          sorting: true,
          paging: true,

          data: data,
          fields: fields
        });
      });
    }
  };
})(jQuery, Drupal)