<?php

namespace Drupal\test_june_2021\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FrontForm.
 */
class FrontForm extends FormBase
{

  /**
   * Drupal\test_june_2021\Services\TestServiceInterface definition.
   *
   * @var \Drupal\test_june_2021\Services\TestServiceInterface
   */
    protected $testService;

  /**
   * {@inheritdoc}
   */
    public static function create(ContainerInterface $container)
    {
        $instance = parent::create($container);
        $instance->testService = $container->get('test_june_2021.default');
        return $instance;
    }

  /**
   * {@inheritdoc}
   */
    public function getFormId()
    {
        return 'test_front_form';
    }

  /**
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#prefix'] = '<div id="test-form" class="test-form">';
        $form['#suffix'] = '</div>';
        $form['name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Name'),
          '#maxlength' => 128,
          '#pattern' => '[a-zA-Z0-9\s]+',
          '#description' => $this->t('Only alphanumeric characters is accepted'),
          '#size' => 32,
          '#weight' => '0',
          '#required' => true,
        ];
        $form['id'] = [
          '#type' => 'number',
          '#title' => $this->t('Id'),
          '#weight' => '1',
          '#required' => true,
        ];
        $form['birth_date'] = [
          '#type' => 'date',
          '#title' => $this->t('Birth date'),
          '#description' => $this->t('In case datepicker is not showed, please provide a date like 15/08/2021'),
          '#weight' => '2',
        ];
        $form['role'] = [
          '#type' => 'select',
          '#title' => $this->t('Role'),
          '#options' => [
            'Administrator' => $this->t('Administrator'),
            'Webmaster' => $this->t('Webmaster'),
            'Developer' => $this->t('Developer')
            ],
          "#empty_option"=>t('- Role -'),
          '#weight' => '3',
        ];
        $form['submit'] = [
          '#type' => 'submit',
          '#weight' => '4',
          '#value' => $this->t('Submit'),
          '#ajax' => [
            'callback' => '::sendData',
            'disable-refocus' => false,
            'event' => 'click',
            'wrapper' => 'test-form',
            'progress' => [
              'type' => 'throbber',
              'message' => $this->t('Sending...'),
            ],
          ],
        ];
        return $form;
    }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $idNumber = $form_state->getValue('id');
        if ($this->testService->idExists($idNumber)) {
            $form_state->setErrorByName('id', $this->t('ID already exists'));
        }
        parent::validateForm($form, $form_state);
    }

  /**
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {

    // Get timestamp
    // $birthDate = $form_state->getValue('birth_date');
    // $date = \DateTime::createFromFormat('Y-m-d',$birthDate);

    // $newUserRow = [
    //   'name' => $form_state->getValue('name'),
    //   'user_id' => $form_state->getValue('id'),
    //   'birth' => $date ? $date->getTimestamp() : NULL,
    //   'role' => $this->testService->getRoleValue($form_state->getValue('role')),
    // ];

    // if($this->testService->createRow($newUserRow))
    //   $this->testService->messenger->addMessage($this->t('Data saved'));
    // else
    //   $this->testService->messenger->addMessage($this->t('Data not saved'));
    }

  /**
   * {@inheritdoc}
   */
    public function sendData(array &$form, FormStateInterface $form_state)
    {
        if (!$form_state->hasAnyErrors()) {
          // Get timestamp
            $birthDate = $form_state->getValue('birth_date');
            $date = \DateTime::createFromFormat('Y-m-d', $birthDate);
              $newUserRow = [
                'name' => $form_state->getValue('name'),
                'user_id' => $form_state->getValue('id'),
                'birth' => $date ? $date->getTimestamp() : null,
                'role' => $form_state->getValue('role'),
                'status' => $this->testService->getRoleValue($form_state->getValue('role')),
              ];

              if ($this->testService->createRow($newUserRow)) {
                    $this->testService->messenger->addMessage($this->t('Data saved'));
              } else {
                    $this->testService->messenger->addMessage($this->t('Data not saved'));
              }
              $form_state->setREbuild();
        }
        return $form;
    }
}
