<?php

namespace Drupal\test_june_2021\Services;

use Drupal\test_june_2021\Services\TestServiceInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class TestService.
 */
class TestService implements TestServiceInterface
{

  /**
   * Drupal\Core\Logger\LoggerChannel definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
    public $logger;

  /**
   * Drupal\Core\Messenger\Messenger definition.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
    public $messenger;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
    public $connection;

  /**
   * Drupal\Core\Datetime\DateFormatter definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
    public $dateFormatter;

  /**
   * Relations of columns for table
   */
    private $fieldsTable = [
    'uid' => 'Uid',
    'name' => 'Name',
    'user_id' => 'ID',
    'birth' => 'Birthdate',
    'role' => 'Role',
    'status' => 'Status',
    ];

  /**
   * Required fields for rest API
   */
    private $requiredFieldsRest = [
    'name' => [
      'field' => 'name',
      'pattern' => '/^[a-zA-Z\s0-9]+$/',
      'message' => 'The name @value does not meet the requirements',
    ],
    'user_id' => [
      'field' => 'user_id',
      'pattern' => '/^[0-9\s]+$/',
      'message' => 'The user id @value does not meet the requirements. Only numbers is accepted',
    ],
    ];

  /**
   * Relations of columns for table
   */
    private $optionalFieldsRest = [
    'birth' => [
      'field' => 'birth',
      'pattern' => '/^[0-9]+$/',
      'message' => 'The birthdate must be a timestamp',
    ],
    'role' => [
      'field' => 'role',
      'pattern' => '/^[a-zA-Z]+$/',
      'message' => 'The role @value does not meet the requirements',
    ],
    ];

  /**
   * Constructs a new TestService object.
   */
    public function __construct(LoggerChannel $logger, Messenger $messenger,
    Connection $connection, DateFormatter $dateFormatter)
    {
        $this->logger = $logger;
        $this->messenger = $messenger;
        $this->connection = $connection;
        $this->dateFormatter = $dateFormatter;
    }

    public function createRow(array $data)
    {
        return $this->connection->insert('example_users')
          ->fields($data)
          ->execute();
    }

    public function getRoleValue(string $formValue)
    {
        switch ($formValue) {
            case 'Administrator':
                return 1;
            break;
            default:
                return 0;
            break;
        }
    }

    public function idExists(string $id)
    {
        $results = $this->connection->select('example_users', 'u')
          ->fields('u')
          ->condition('u.user_id', $id)
          ->execute()
          ->fetchAll();
        if (count($results)) return true;
        return false;
    }

    public function getIndexData(array $fields = ['uid', 'name', 'user_id', 'birth', 'role', 'status'])
    {
        $results = $this->connection->select('example_users', 'u')
          ->fields('u', $fields)
          ->execute()
          ->fetchAll();

        $arrayTableData = [];
        foreach ($results as $i => $row) {
            $newRow = [];
            foreach ($row as $key => $value) {
                $keyNewArray = t($this->fieldsTable[$key])->__toString();
                if ($key == 'birth') {
                    if ($value != 0)
                        $dateFormatted = $this->dateFormatter->format(intval($value), 'custom', 'Y/m/d');
                    else
                        $dateFormatted = t('No date added')->__toString();
                        $newRow[$keyNewArray] = $dateFormatted;
                } elseif ($key == 'status') {
                    if ($value == 1)
                        $newRow[$keyNewArray] = true;
                    else
                    $newRow[$keyNewArray] = false;
                } else
                    $newRow[$keyNewArray] = $value;
              }
              $arrayTableData[] = $newRow;
        }
        return $arrayTableData;
    }

    public function getFieldsTable()
    {
        $roles = [
          [
            'Name' => "",
            'Id' => 0
          ],
          [
            'Name' => t('Administrator')->__toString(),
            'Id' => 'Administrator'
          ],
          [
            'Name' => t('Webmaster')->__toString(),
            'Id' => 'Webmaster'
          ],
          [
            'Name' => t('Developer')->__toString(),
            'Id' => 'Developer'
          ]
        ];

        $fields = [
          [
            'name' => t('Uid')->__toString(),
            'type' => "number",
            'width' => 30,
          ],
          [
            'name' => t('Name')->__toString(),
            'type' => 'text',
            'validate' => 'required',
            'width' => 200,
          ],
          [
            'name' => t('ID')->__toString(),
            'type' => "number",
            'validate' => 'required',
            'width' => 100,
          ],
          [
            'name' => t('Birthdate')->__toString(),
            'type' => 'text',
            'width' => 100,
          ],
          [
            'name' => t('Role')->__toString(),
            'type' => "select",
            'items' => $roles,
            'valueField' => "Id",
            'textField' => "Name",
            'width' => 100,
          ],
          [
            'name' => t('Status')->__toString(),
            'type' => "checkbox",
            'title' => t('Status')->__toString(),
            'sorting' => false
          ]
        ];
        return $fields;
    }

    public function getPlainIndexData($rowId = null, array $fields = ['uid', 'name', 'user_id', 'birth', 'role']){
        $query = $this->connection->select('example_users', 'u')
          ->fields('u', $fields);

        if (!is_null($rowId))
          $query->condition('uid', $rowId);

        $results = $query->execute()
          ->fetchAll();
        return $results;
    }

    public function processRestCreateRow(array $data)
    {
        $errors = $this->checkRestRow($data);
        if (count($errors)) {
            return $this->throwFieldErrorResponse($errors);
        }
        // Checking role data
        $this->checkRoleData($data);
        $newUserRow = $this->createRow($data);
        $response = [
          'status' => 200,
          'message' => 'Row created',
          'data' => [
            'body' => $data,
            'id_new_row' => $newUserRow,
          ],
        ];
        return new JsonResponse($response);
    }

    function checkRestRow(array $data, string $method = 'new')
    {
        $errors = [];
        if ($method != 'delete') {
            foreach ($this->requiredFieldsRest as $fieldName => $fieldValidation) {
                if (!isset($data[$fieldName])) {
                    $errors[] = t('@field_name is required', ['@field_name' => $fieldName]);
                } else {
                    if (!preg_match($fieldValidation['pattern'], $data[$fieldName], $matches)) {
                        $errors[] = t($fieldValidation['message'], ['@value' => $data[$fieldName]]);
                    }
                }
            }
        }
        if ($method === 'new') {
            if ($this->idExists($data['user_id']))
                $errors[] = t('The ID @id already exists', ['@id' => $data['user_id']]);
        }
        if ($method === 'update' || $method === 'delete') {
            if (!$this->idExists($data['user_id']))
                $errors[] = t('The ID @id does not exist', ['@id' => $data['user_id']]);
        }
        if ($method != 'delete') {
            foreach ($this->optionalFieldsRest as $fieldName => $fieldValidation) {
                if (isset($data[$fieldName])) {
                    if (!preg_match($fieldValidation['pattern'], $data[$fieldName], $matches)) {
                        $errors[] = t($fieldValidation['message'], ['@value' => $data[$fieldName]]);
                    }
                }
            }
        }
        return $errors;
    }

    public function throwFieldErrorResponse(array $errors)
    {
        return new JsonResponse([
          'status' => 406,
          'message' => t('Field error'),
          'errors' => $errors,
        ], 406);
    }

    public function processRestUpdateRow(array $data)
    {
        $errors = $this->checkRestRow($data, 'update');
        if (count($errors)) {
            return $this->throwFieldErrorResponse($errors);
        }
        $this->checkRoleData($data);
        $updatedRows = $this->updateRow($data);
        $response = [
          'status' => 200,
          'message' => 'Row updated',
          'data' => [
            'body' => $data,
            'rows_affected' => $updatedRows,
          ],
        ];
        return new JsonResponse($response);
    }

    public function updateRow(array $data)
    {
        $numUpdated = $this->connection->update('example_users')
          ->fields($data)
          ->condition('user_id', $data['user_id'])
          ->execute();

        return $numUpdated;
    }

    public function checkRoleData(array &$data)
    {
        if (isset($data['role'])) $data['status'] = $this->getRoleValue($data['role']);
        else $data['status'] = 0;
    }

    public function processRestDeleteRow(array $data)
    {
        $errors = $this->checkRestRow($data, 'delete');
        if (count($errors)) {
            return $this->throwFieldErrorResponse($errors);
        }
        $deletedRows = $this->deleteRow($data);
        $response = [
          'status' => 200,
          'message' => 'Row deleted',
          'data' => [
            'body' => $data,
            'rows_affected' => $deletedRows,
          ],
        ];
        return new JsonResponse($response);
    }

    public function deleteRow(array $data)
    {
        $numUpdated = $this->connection->delete('example_users')
          ->condition('user_id', $data['user_id'])
          ->execute();

        return $numUpdated;
    }

    public function deleteRowById(int $rowId)
    {
        $numUpdated = $this->connection->delete('example_users')
          ->condition('uid', $rowId)
          ->execute();

        return $numUpdated;
    }
}
