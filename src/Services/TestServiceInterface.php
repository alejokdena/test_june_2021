<?php

namespace Drupal\test_june_2021\Services;

/**
 * Interface TestServiceInterface.
 */
interface TestServiceInterface
{

  /**
   * Function to create a new row in example_users table
   *
   * @param array $data Row data
   *
   * @return int id of row created, NULL otherwise
   */
    function createRow(array $data);

  /**
   * Function to get Role value to save in DB
   *
   * @param string $formValue Form value for role
   *
   * @return int Value of role for DB
   */
  function getRoleValue(string $formValue);

  /**
   * Function to check if id exists in DB
   *
   * @param string $id ID
   *
   * @return boolean True if exists, FALSE otherwise
   */
  function idExists(string $id);

  /**
   * Function to get all Data from example_users table
   * for index route
   *
   * @param array $fields Fields to get from table
   *
   * @return array all data from table
   */
  function getIndexData(array $fields = ['uid', 'name', 'user_id', 'birth', 'role']);

  /**
   * Function to get fields header of table
   *
   * @return array definition of fields for jsGrid Library
   */
  function getFieldsTable();

  /**
   * Function to get all Data from example_users table
   *
   * @param array $fields Fields to get from table
   *
   * @return array all data from table
   */
  function getPlainIndexData($rowId = null, array $fields = ['uid', 'name', 'user_id', 'birth', 'role', 'status']);

  /**
   * Function to process data for rest create service
   *
   * @param array $data from service
   */
  function processRestCreateRow(array $data);

  /**
   * Function to check data for rest create service
   *
   * @param array $data from service
   */
  function checkRestRow(array $data, string $method = 'new');

  /**
   * Function to return response for errors in fields
   *
   * @param array $errors
   *
   * @return array $response
   */
  function throwFieldErrorResponse(array $errors);

  /**
   * Function to process data for rest update service
   *
   * @param array $data from service
   */
  function processRestUpdateRow(array $data);

  /**
   * Function to update a new row in example_users table
   *
   * @param array $data Row data
   *
   * @return int id of row created, NULL otherwise
   */
  function updateRow(array $data);

  /**
   * Function to change the value of role for storage
   *
   * @param array $data Row data
   */
  function checkRoleData(array &$data);

  /**
   * Function to process data for rest delete service
   *
   * @param array $data from service
   */
  function processRestDeleteRow(array $data);

  /**
   * Function to delete a new row in example_users table
   *
   * @param array $data Row data
   *
   * @return int id of row created, NULL otherwise
   */
  function deleteRow(array $data);

  /**
   * Function to delete users row by row id
   *
   * @param int $rowId Row ID
   *
   * @return int Rows affected
   */
  function deleteRowById(int $rowId);
}
